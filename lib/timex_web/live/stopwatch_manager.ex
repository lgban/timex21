defmodule TimexWeb.StopwatchManager do
  use GenServer

  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    GenServer.cast(ui, {:set_time_display, "00:00.00" })
    {:ok, %{ui_pid: ui, count: ~T[00:00:00.00], st1: Working, st2: Paused, timer: nil}}
  end

  def handle_info(msg, state) do

  end

  def handle_info(:"bottom-right", %{st2: Paused} = state) do
    timer = Process.send_after(self(), Counting2Counting, 10)
    {:noreply, %{ state | st2: Counting, timer: timer}}
  end

  def handle_info(:"bottom-right", %{st2: Counting, timer: timer} = state) do
    if timer != nil do
      Process.cancel_timer(timer)
    end
    {:noreply, %{ state | st2: Paused}}
  end

  def handle_info(Counting2Counting, %{ui_pid: ui, st2: Counting, count: count} = state) do
    timer = Process.send_after(self(), Counting2Counting, 10)
    count = Time.add(count, 10, :millisecond)
    label = count |> Time.add(10, :millisecond) |> Time.to_string |> String.slice(3, 8)
    GenServer.cast(ui, {:set_time_display, label})
    IO.inspect("Counting")
    {:noreply, %{state | st2: Counting, timer: timer, count: count}}
  end
end
